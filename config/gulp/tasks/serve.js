var runSequence = require('run-sequence');
var nodemon = require('gulp-nodemon');

var envConfig = require('../utils/env');

if (envConfig.ENV === envConfig.ENVS.DEV)
{
    var gulp = require('gulp');
    var config = require('../config')();
    var bs = require("browser-sync");

    var nodemonOptions = {
        script: 'server/app.js',
        ext: 'js',
        env: { 'NODE_ENV': 'development' },
        verbose: false,
        ignore: [],
        watch: ['server/*']
    };

    gulp.task('start-server', function () {
        nodemon(nodemonOptions)
            .on('restart', function () {
                console.log('restarted!')
            });
    });


    function startBrowsersync (config)
    {
        bsIns = bs.create();
        bsIns.init(config);
        bsIns.reload();
    }

    /* Start live server dev mode */
    gulp.task('serve-dev', ['start-server'], function ()
    {
        runSequence(
            ['sass', 'tsc-app'],
            ['html', 'css'],
            ['watch-sass', 'watch-ts', 'watch-html', 'watch-css'], function() {
            startBrowsersync(config.browserSync.dev);
        });
    });

    /* Start live server production mode */
    gulp.task('serve-build', ['build', 'start-server'], function ()
    {
        startBrowsersync(config.browserSync.prod);
    });
}
