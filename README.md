# Angular4 POC

## Prerequisites
You need to have [Node.js and npm](https://nodejs.org/en/)
- Support Node v6.9 - latest
- Support npm v3 - latest

## Installation


Go to the  directory and install the packages:
```bash
npm install
```

## Start
Let's start up the server, run:
```bash
npm start
```

and done! The browser will popup and you can check Angualar 2 POC!

## Testing

### Unit testing

```bash
npm test
```
and it'll compile all TypeScript files, start Karma, then remap Istanbul coverage so that it shows TypeScript coverage, not the transpiled JavaScript coverage.

### E2E testing
Firstly start the server:
```bash
npm start
```
To begin testing, run:
```bash
npm run e2e
```
and it'll compile all E2E spec files in `/src/test/e2e/*.spec.ts`, boot up Selenium server then launches Chrome browser.

## Production
> All build tasks will run the `gulp test`, the bundle will only be created if the test passed.


You can create production build by running:
```bash
npm run build
```
or you can create production build and then serve it using Browsersync by running:
```bash
npm run serve-build

```
