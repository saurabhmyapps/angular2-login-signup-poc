import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { User } from '../models/user';
import { Configuration } from '../config/configuration';

@Injectable()
export class UserService {
    private apiHost = '';
    private userUrl = 'api/users';  // URL to web api
    private authUser = 'auth/local';

    constructor(private http: Http, private config: Configuration) {
        this.apiHost = this.config.apiHost;
    }

    getUser(): Promise<User> {
        let token = localStorage.getItem('token');
        let headers = new Headers({
            'authorization': `Bearer ${token}`
        });
        return this.http.get(`${this.apiHost}/${this.userUrl}/me`, {headers: headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    save(user: User): Promise<User> {
        if (user._id) {
            return this.put(user);
        }
        return this.post(user);
    }

    login(user: User) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiHost}/${this.authUser}`;

        return this.http
            .post(url, JSON.stringify(user), {headers: headers})
            .toPromise()
            .then(response => {
                let userObj = response.json();
                console.log(userObj);
                if (userObj && userObj.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('token', userObj.token);
                    return userObj.token;
                }
            })
            .catch(this.handleError);
    }

    updateDob(user: User) {
        let token = localStorage.getItem('token');
        let headers = new Headers({
            'authorization': `Bearer ${token}`
        });
        return this.http.put(`${this.apiHost}/${this.userUrl}/${user._id}/dob`, {dob: user.dob}, {headers: headers})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
    }

    private post(user: User): Promise<User> {
        let headers = new Headers({
            'Content-Type': 'application/json'
        });
        let url = `${this.apiHost}/${this.userUrl}`;
        return this.http
            .post(url, JSON.stringify(user), {headers: headers})
            .toPromise()
            .then(response => response.json().data)
            .catch(this.handleError);
    }

    private put(user: User) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let url = `${this.apiHost}/${this.userUrl}/${user._id}`;

        return this.http
            .put(url, JSON.stringify(user), {headers: headers})
            .toPromise()
            .then(() => user)
            .catch(this.handleError);
    }

    private handleError(error: any) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
