import { async, fakeAsync, getTestBed, TestBed, inject, tick } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions, HttpModule, Jsonp, Http, Response, ResponseOptions, XHRBackend } from '@angular/http';
import { UserService } from './user.service';
import { User } from '../models/user';
import { Configuration } from '../config/configuration';

describe('Login: UserService', () => {
    let service: UserService;
    let backend: MockBackend;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                User,
                UserService,
                Configuration,
                MockBackend,
                BaseRequestOptions,
                {
                    provide: Http,
                    useFactory: (backendObj, options) => new Jsonp(backend, options),
                    deps: [MockBackend, BaseRequestOptions]
                }
            ]
        });

        backend = TestBed.get(MockBackend);

        service = TestBed.get(UserService);
    });

    it('should return an token', fakeAsync(() => {
        let response = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9';

        // When the request subscribes for results on a connection, return a fake response
        backend.connections.subscribe(connection => {
            connection.mockRespond(new Response(<ResponseOptions>{
                body: response
            }));
        });

        // Perform a request and make sure we get the response we expect
        let user: User = JSON.parse('{ "email":"test@test.com", "password": "123456" }');
        let serv = service.login(user);
        // .then(function (data) {
        //     expect(data).toEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9');
        // });
        tick(1000);
        serv.then(function (data) {
            expect(data).toEqual('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9');
        });

    }));
});
