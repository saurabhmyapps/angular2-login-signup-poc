import { Component, OnInit, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'as-navigation',
    templateUrl: 'navigation.component.html'
})

export class NavigationComponent implements OnInit {
    @Input() isUserLoggedIn: string;
    isLoggedIn = '';

    ngOnInit() {
        this.isLoggedIn = this.isUserLoggedIn;
    }
}
