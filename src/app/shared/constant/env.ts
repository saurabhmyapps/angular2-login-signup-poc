/**
 * THIS FILE IS GENERATED by `gulp env` command from `env.json`
 * Generated on Wed Aug 16 2017 07:12:13 GMT+0530 (IST)
 *
 * Make sure the keys in `env.model.ts` exist in `env.json`
 * otherwise it'll throw message like this
 * Property '<missing-key>' is missing in type '{}'
 *
 * Feel free to modify for direct updates in development
 */

import { AppEnv } from './env.model';

export const ENV: AppEnv = {
};
