import { UserService } from './services/user.service';
import { Configuration } from './config/configuration';
export const APP_PROVIDERS = [
    Configuration,
    UserService
];
