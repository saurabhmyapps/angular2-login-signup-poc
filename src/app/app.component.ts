import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'as-main-app',
    template: '<router-outlet></router-outlet>'
})
export class AppComponent {

}
