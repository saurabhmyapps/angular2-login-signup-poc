export class User {
    _id: string;
    name: string;
    email: string;
    dob: number;
    password: string;
}
