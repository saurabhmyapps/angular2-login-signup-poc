import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { ProfileComponent } from './index';

@NgModule({
    declarations: [
        ProfileComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    exports: [
        ProfileComponent
    ]
})
export class ProfileModule {
}
