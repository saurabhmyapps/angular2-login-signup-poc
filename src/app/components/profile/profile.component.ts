import { Component, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'as-profile',
    templateUrl: 'profile.html'
})

export class ProfileComponent implements OnChanges {
    @Input() user: any;
    @Output() onCancel = new EventEmitter<any>();

    errorMsg: String = '';
    error: any;
    userdob: any = '';



    constructor(private userService: UserService) { }

    ngOnChanges() {
        this.userdob = this.user.dob;
    }
    cancelUpdate() {
        this.onCancel.emit(0);
        this.user = {};
    }
    update() {
        this.user.dob = this.userdob;
        this.userService
            .updateDob(this.user)
            .then(user => {

                    this.onCancel.emit(0);
                    this.user = {};
                },
                (error) => {
                    let json = JSON.parse(error._body);
                    this.errorMsg = json.errors.email.message;
                })
            .catch(error => this.error = error);
    }
}
