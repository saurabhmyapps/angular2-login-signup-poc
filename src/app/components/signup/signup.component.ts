import { Component } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'as-signup',
    templateUrl: 'signup.html'
})
export class SignupComponent {
    user: User;
    error: any;
    registerSuccess: Boolean = false;
    errorMsg: any = '';

    constructor(private userService: UserService) {
        this.user = new User();
    }

    register() {
        this.userService
            .save(this.user)
            .then(user => {
                    this.errorMsg = '';
                    this.registerSuccess = true;
                },
                (error) => {
                    let json = JSON.parse(error._body);
                    this.registerSuccess = false;
                    this.errorMsg = json.errors.email.message;
                })
            .catch(error => this.error = error);
    }

    goBack() {
        window.history.back();
    }
}
