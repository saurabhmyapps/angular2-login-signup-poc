import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { SignupComponent } from './index';
import { NavigationModule } from '../../shared';

@NgModule({
    declarations: [
        SignupComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        NavigationModule
    ],
    exports: [
        SignupComponent
    ]
})
export class SignupModule {
}
