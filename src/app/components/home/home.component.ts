import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'as-home',
    templateUrl: 'home.html'
})
export class HomeComponent {
    title = 'Angular 4 Project POC';
}
