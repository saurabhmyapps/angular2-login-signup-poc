import { HomeComponent } from './index';
import { NavigationModule } from '../../shared/navigation/index';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  RouterTestingModule
} from '@angular/router/testing';
import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('HomeComponent', function () {
  let de: DebugElement;
  let comp: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [ RouterTestingModule, NavigationModule ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('.center'));
  });

  it('should create component', () => expect(comp).toBeDefined() );
  it('should have expected css class .center', () => {
    fixture.detectChanges();
    const center = de.nativeElement;
    expect(center.innerText).toMatch('HCL Technologies Project POC');
  });
});

