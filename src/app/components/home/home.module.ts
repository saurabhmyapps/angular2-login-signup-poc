import { NgModule } from '@angular/core';
import { HomeComponent } from './index';
import { NavigationModule } from '../../shared';

@NgModule({
    declarations: [
        HomeComponent
    ],
    imports: [
        NavigationModule
    ],
    exports: [
        HomeComponent
    ]
})
export class HomeModule {
}
