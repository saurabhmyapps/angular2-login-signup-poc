import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule, FormBuilder }    from '@angular/forms';
import { By }           from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { LoginComponent } from './index';
import { NavigationModule } from '../../shared/navigation/index';
import { APP_PROVIDERS } from '../../app.providers';

describe('LoginComponent', function () {
  let de: DebugElement;
  let comp: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [ RouterTestingModule, NavigationModule, ReactiveFormsModule, FormsModule, HttpModule ],
      providers: [ APP_PROVIDERS, FormBuilder ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('.center'));
  });

  it('should create component', () => expect(comp).toBeTruthy() );
});

