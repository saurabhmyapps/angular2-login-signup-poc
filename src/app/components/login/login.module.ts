import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { LoginComponent } from './index';
import { NavigationModule } from '../../shared';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        NavigationModule
    ],
    exports: [
        LoginComponent
    ]
})
export class LoginModule {
}
