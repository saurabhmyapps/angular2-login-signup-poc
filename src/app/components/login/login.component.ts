import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'as-login',
    templateUrl: 'login.html'
})
export class LoginComponent implements OnInit {
    user: any =  {};
    errorMsg: String = '';
    error: any;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService) {
    }

    ngOnInit() {
        // reset login status
        this.userService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams.returnUrl || 'dashboard';
    }
    login() {
        console.log(this.user);
        this.userService
            .login(this.user)
            .then(user => {
                console.log(user);
                // this.router.navigate([this.returnUrl]);
            }, error => {
                let json = JSON.parse(error._body);
                this.errorMsg = json.message;
            })
            .catch(error => this.error = error);
    }
}
