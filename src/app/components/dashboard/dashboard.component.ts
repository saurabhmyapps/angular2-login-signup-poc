import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'as-dashboard',
    templateUrl: 'dashboard.html'
})
export class DashboardComponent implements OnInit {
    currentUser: User;
    isUpdate: any = 0;

    constructor(
        private router: Router,
        private userService: UserService) {
        this.currentUser = new User();
    }

    ngOnInit() {
        this.getUserDetail();
    }

    updateDob() {
        this.isUpdate = 1;
    }

    onCancel() {
        this.isUpdate = 0;
    }

    private getUserDetail() {
        this.userService.getUser().then(user => { this.currentUser = user; });
    }
}
