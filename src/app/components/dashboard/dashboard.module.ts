import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from './index';
import { NavigationModule } from '../../shared';
import { ProfileModule } from '../profile/profile.module';

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        BrowserModule,
        NavigationModule,
        ProfileModule
    ],
    exports: [
        DashboardComponent
    ]
})
export class DashboardModule {
}
