import { Routes, RouterModule } from '@angular/router';

import { HomeRoutes } from './components/home/index';
import { LoginRoutes } from './components/login/index';
import { DashboardRoutes } from './components/dashboard/index';
import { SignupRoutes } from './components/signup/index';

const appRoutes: Routes = [
    ...HomeRoutes,
    ...LoginRoutes,
    ...DashboardRoutes,
    ...SignupRoutes
];

export const appRoutingProviders: any[] = [

];

export const routing = RouterModule.forRoot(appRoutes);
