/**
 * Express configuration
 */

'use strict';

var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var path = require('path');
var config = require('./environment');
var passport = require('passport');
var cors = require('cors');


module.exports  =  function(app) {
	var env = app.get('env');
    app.use(cors());

	app.set('appPath', path.join(config.root, 'public'));
	app.use(express.static(app.get('appPath')));
	app.use(morgan('dev'));
	app.use(bodyParser.urlencoded({ extended: false }));
  	app.use(bodyParser.json());
  	app.use(cookieParser());
  	app.use(passport.initialize());

// // for system.js to work. Can be removed if bundling.
     app.use(express.static(path.join(__dirname, '/../public')));
     app.use(express.static(path.join(__dirname, '/../node_modules')));


}




