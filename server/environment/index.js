'use strict'

var path = require('path');
var _ = require('lodash');


var common = {
	env: process.env.NODE_ENV,
	// Root path of server
  	root: path.normalize(`${__dirname}/../..`),
	// Server port
  	port: process.env.PORT || 4000,
  	// Server IP
  	ip: process.env.IP || '0.0.0.0',
	// Secret for session, you will want to change this and make it an environment variable
	secrets: {
	session: 'mywork-secret'
	},
	// MongoDB connection options
	mongo: {
		options: {
			db: {
				safe: true
			}
		}
	},
}

module.exports = _.merge(
		common,
		require(`./${process.env.NODE_ENV}.js`) || {}
	);
