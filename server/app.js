'use strict'

// Set default node environment to development
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var mongoose =  require('mongoose');
var config = require('./environment');
var http = require('http');


// Connect to MongoDB
mongoose.Promise = global.Promise;
mongoose.connect(config.mongo.uri, config.mongo.options);

// Setup server
var app = express();
var server = http.createServer(app);

require('./express')(app);
require('./routes')(app);

// Start server
server.listen(config.port, config.ip, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});
