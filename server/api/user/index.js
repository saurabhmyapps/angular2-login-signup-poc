'use strict';

var {Router} = require('express');
var controller = require('./user.controller');
var auth = require('../../auth/auth.service');

var router = new Router();

router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/dob', auth.isAuthenticated(), controller.updateDob);
router.post('/', controller.create);

module.exports = router;
