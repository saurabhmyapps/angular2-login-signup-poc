'use strict';

var User = require('./user.model');
var config = require('../../environment');
var jwt = require('jsonwebtoken');

function validationError(res, statusCode) {
  statusCode = statusCode || 422;
  return function(err) {
    return res.status(statusCode).json(err);
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    return res.status(statusCode).send(err);
  };
}


/**
 * Creates a new user
 */
exports.create = function create(req, res) {
  var newUser = new User(req.body);
  newUser.role = 'user';
  newUser.save()
    .then(function(user) {
      var token = jwt.sign({ _id: user._id }, config.secrets.session, {
        expiresIn: 60 * 60 * 5
      });
      res.json({ token });
    })
    .catch(validationError(res));
}



/**
 * Change a user dob
 */
exports.updateDob = function updateDob(req, res) {
  var userId = req.user._id;
  var dob = req.body.dob;

  return User.findById(userId).exec()
    .then(user => {
        user.dob = dob;
        return user.save()
          .then(() => {
            res.status(204).end();
          })
          .catch(validationError(res));
    });
}

/**
 * Get my info
 */
exports.me = function me(req, res, next) {
  var userId = req.user._id;

  return User.findOne({ _id: userId }, '-salt -password').exec()
    .then(user => { // don't ever give out the password or salt
      if(!user) {
        return res.status(401).end();
      }
      res.json(user);
    })
    .catch(err => next(err));
}
